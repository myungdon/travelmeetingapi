package com.nh.travelmeetingapi.model.member;

import com.nh.travelmeetingapi.enums.member.Grade;
import com.nh.travelmeetingapi.enums.member.State;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class MemberItem {
    private Long id;

    private String name;

    private Boolean isNormal;

    private String account;

    private Long password;

    private LocalDate birthday;

    private String phoneNumber;

    private LocalDateTime dateJoin;

    private Boolean isMan;

    private String eMail;

    private String address;

    private String grade;

    private String reason;

    private String state;

    private String etcMemo;
}
