package com.nh.travelmeetingapi.enums.pay;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PayBy {
    CARD("카드")
    ,KAKAO_PAY("카카오페이")
    ,MAKE_DEPOSIT("무통장 입금")
    ,PHONE("핸드폰 결제");

    private final String name;
}
