package com.nh.travelmeetingapi.repository;

import com.nh.travelmeetingapi.entity.Pay;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PayRepository extends JpaRepository<Pay, Long> {
}
