package com.nh.travelmeetingapi.entity.matching;

import com.nh.travelmeetingapi.enums.matching.ThemeCategory;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class ThemeMatching {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "matchingId", nullable = false)
    private Matching matching;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private ThemeCategory themeCategory;

    @Column(nullable = false)
    private LocalDate dateAsk;
}
