package com.nh.travelmeetingapi.service;

import com.nh.travelmeetingapi.entity.Board;
import com.nh.travelmeetingapi.entity.Member;
import com.nh.travelmeetingapi.model.board.BoardCreateRequest;
import com.nh.travelmeetingapi.repository.BoardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class BoardService {
    private final BoardRepository boardRepository;

    public void setBoard(Member member, BoardCreateRequest request) {
        Board addData = new Board();
        addData.setMember(member);
        addData.setCategory(request.getCategory());
        addData.setDateWrite(LocalDate.now());
        addData.setTitle(request.getTitle());
        addData.setContent(request.getContent());

        boardRepository.save(addData);
    }
}
