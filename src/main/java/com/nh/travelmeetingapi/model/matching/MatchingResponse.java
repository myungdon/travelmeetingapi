package com.nh.travelmeetingapi.model.matching;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class MatchingResponse {
    private Long id;
    private String matchingCode;
    private Double matchingPrice;
    private String matchingCategory;
    private LocalDate dateStart;
    private LocalDate dateEnd;
    private String address;
    private String pension;
}
