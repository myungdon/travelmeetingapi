package com.nh.travelmeetingapi.enums.board;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Category {
    RECOMMEND("여행추천")
    ,MATCHING_REVIEW("여행리뷰");

    private final String name;
}
