package com.nh.travelmeetingapi.repository;

import com.nh.travelmeetingapi.entity.Coupon;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CouponRepository extends JpaRepository<Coupon, Long> {
}
