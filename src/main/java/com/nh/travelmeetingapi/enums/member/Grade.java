package com.nh.travelmeetingapi.enums.member;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Grade {
    BRONZE("브론즈 등급")
    ,SILVER("실버 등급")
    ,GOLD("골드 등급");

    private final String name;
}
