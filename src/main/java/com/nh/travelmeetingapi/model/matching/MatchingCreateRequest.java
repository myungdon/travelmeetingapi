package com.nh.travelmeetingapi.model.matching;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class MatchingCreateRequest {
    private String matchingCode;
    private Double matchingPrice;
    private LocalDate dateEnd;
}
