package com.nh.travelmeetingapi.enums.coupon;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum IsUsed {
    Not_USED(false)
    ,IS_USED(true);

    private final Boolean isUsed;
}
