package com.nh.travelmeetingapi.service;

import com.nh.travelmeetingapi.entity.Member;
import com.nh.travelmeetingapi.enums.member.Grade;
import com.nh.travelmeetingapi.enums.member.State;
import com.nh.travelmeetingapi.model.member.MemberDupCheckResponse;
import com.nh.travelmeetingapi.model.member.MemberItem;
import com.nh.travelmeetingapi.model.member.MemberJoinRequest;
import com.nh.travelmeetingapi.model.member.MemberResponse;
import com.nh.travelmeetingapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;


@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData(Long id) {return memberRepository.findById(id).orElseThrow();}

    public MemberDupCheckResponse getMemberIdDupCheck(String account){
        MemberDupCheckResponse response = new MemberDupCheckResponse();
        response.setIsNew(isNewMember(account));

        return response;
    }

    /**
     * 신규 아이디인지 확인한다
     *
     * @param account 아이디
     * @return  true 신규아이디 / false 중복아이디
     */

    /**
     *
     * @param request 회원가입창에서 고객이 작성한 정보
     * @throws Exception 아이디 중복 확인 비밀번호랑 비밀번호 확인이 일치 하지 않을 경우
     */
    public void setMember(MemberJoinRequest request) throws Exception {
        if (!isNewMember(request.getAccount())) throw new Exception();
        if (!request.getPassword().equals(request.getPasswordRe())) throw new Exception();

        Member addData = new Member();
        addData.setName(request.getName());
        addData.setIsNormal(true);
        addData.setAccount(request.getAccount());
        addData.setPassword(request.getPassword());
        addData.setBirthday(request.getBirthday());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setDateJoin(LocalDateTime.now());
        addData.setIsMan(request.getIsMan());
        addData.setEMail(request.getEMail());
        addData.setAddress(request.getAddress());
        addData.setGrade(Grade.BRONZE);
        addData.setState(State.ACTIVITY);

        memberRepository.save(addData);
    }

    /**
     * 등록된 회원 정보들을 조회
     * @return
     */
    public List<MemberItem> getMember(){
        List<Member> originList = memberRepository.findAll();
        List<MemberItem> result = new LinkedList<>();

        for (Member member : originList ) {
            MemberItem addItem = new MemberItem();
            addItem.setId(member.getId());
            addItem.setName(member.getName());
            addItem.setIsNormal(member.getIsNormal());
            addItem.setAccount(member.getAccount());
            addItem.setPassword(member.getPassword());
            addItem.setBirthday(member.getBirthday());
            addItem.setPhoneNumber(member.getPhoneNumber());
            addItem.setIsMan(member.getIsMan());
            addItem.setEMail(member.getEMail());
            addItem.setAddress(member.getAddress());
            addItem.setGrade(member.getGrade().getName());
            addItem.setReason(member.getReason());
            addItem.setState(member.getState().getName());
            addItem.setEtcMemo(member.getEtcMemo());

            result.add(addItem);
        }
        return result;
    }
    public MemberResponse getMember(long id){
        Member originData = memberRepository.findById(id).orElseThrow();

        MemberResponse response = new MemberResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setAccount(originData.getAccount());
        response.setPassword(originData.getPassword());
        response.setBirthday(originData.getBirthday());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setIsMan(originData.getIsMan());
        response.setEMail(originData.getEMail());
        response.setAddress(originData.getAddress());
        response.setGrade(originData.getGrade().getName());
        response.setReason(originData.getReason());
        response.setState(originData.getState().getName());
        response.setEtcMemo(originData.getEtcMemo());

        return response;
    }
    private boolean isNewMember(String account){
        long dupCount = memberRepository.countByAccount(account);

        return dupCount <= 0;
    }
}
