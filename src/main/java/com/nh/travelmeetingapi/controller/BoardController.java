package com.nh.travelmeetingapi.controller;

import com.nh.travelmeetingapi.entity.Member;
import com.nh.travelmeetingapi.model.board.BoardCreateRequest;
import com.nh.travelmeetingapi.service.BoardService;
import com.nh.travelmeetingapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/board")
public class BoardController {
    private final BoardService boardService;
    private final MemberService memberService;

    @PostMapping("/bulletin/member/{memberId}")
    public String setBoard(@PathVariable long memberId, @RequestBody BoardCreateRequest request){
        Member member = memberService.getData(memberId);
        boardService.setBoard(member, request);

        return "OK";
    }
}
