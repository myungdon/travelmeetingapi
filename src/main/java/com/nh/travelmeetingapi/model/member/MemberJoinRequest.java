package com.nh.travelmeetingapi.model.member;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class MemberJoinRequest {
    private String name;

    private String account;

    private Long password;

    private Long passwordRe;

    private LocalDate birthday;

    private String phoneNumber;

    private Boolean isMan;

    private String eMail;

    private String address;

}

