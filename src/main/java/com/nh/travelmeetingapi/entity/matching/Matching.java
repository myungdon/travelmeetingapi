package com.nh.travelmeetingapi.entity.matching;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Matching {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String matchingCode;

    @Column(nullable = false)
    private Double matchingPrice;

    @Column(nullable = false)
    private String matchingCategory;

    @Column(nullable = false)
    private LocalDate dateStart;

    @Column(nullable = false)
    private LocalDate dateEnd;

    @Column(nullable = false)
    private String address;

    @Column(nullable = false)
    private String pension;
}
