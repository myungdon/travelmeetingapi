package com.nh.travelmeetingapi.model.board;

import com.nh.travelmeetingapi.enums.board.Category;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BoardCreateRequest {
    private Category category;
    private String title;
    private String content;
}
