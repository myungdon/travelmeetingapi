package com.nh.travelmeetingapi.repository;

import com.nh.travelmeetingapi.entity.matching.Matching;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MatchingRepository extends JpaRepository<Matching, Long> {
}
