package com.nh.travelmeetingapi.controller;

import com.nh.travelmeetingapi.model.member.MemberItem;
import com.nh.travelmeetingapi.model.member.MemberJoinRequest;
import com.nh.travelmeetingapi.model.member.MemberResponse;
import com.nh.travelmeetingapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/join")
    public String setMember(@RequestBody MemberJoinRequest request) throws Exception {
        memberService.setMember(request);

        return "Ok";
    }
    @GetMapping("/id/duplicate/check")
    public ResponseEntity<?> getMemberDupCheck(@RequestParam (value = "account")String account )
        throws Exception{
        System.out.println(account);

        if (memberService.getMemberIdDupCheck(account).equals(true)) {
            throw new Exception("이미 사용중인 아이디입니다.");
        } else {
                return ResponseEntity.ok("사용가능한 아이디입니다");
        }
    }

    @GetMapping("/pw/duplicate/check")
    public String getMemberDupChecks(@RequestParam ("password") String password, Model model ){
        model.addAttribute("password", password);

        return "비밀번호 일치함";
    }

    @GetMapping("/all")
    public List<MemberItem> getMembers(){
        return memberService.getMember();
    }
    @GetMapping("/member/{memberId}")
    public MemberResponse getMember(@PathVariable long id){
        return memberService.getMember(id);
    }
}
