package com.nh.travelmeetingapi.enums.matching;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ThemeCategory {
    FISHING("낚시")
    ,HIKING("등산")
    ,CAMPING("캠핑")
    ,HEALING("힐링");

    private final String name;
}
