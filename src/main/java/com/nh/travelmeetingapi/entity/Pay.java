package com.nh.travelmeetingapi.entity;

import com.nh.travelmeetingapi.entity.matching.Matching;
import com.nh.travelmeetingapi.enums.pay.PayBy;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Pay {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "matchingId", nullable = false)
    private Matching matching;

    @Column(nullable = false, length = 50, unique = true)
    private String payCode;

    @Column(nullable = false)
    private LocalDate datePay;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private PayBy payBy;

    @Column(nullable = false)
    private Boolean isCancel;
}
