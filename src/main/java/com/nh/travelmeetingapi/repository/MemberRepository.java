package com.nh.travelmeetingapi.repository;

import com.nh.travelmeetingapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
    long countByAccount(String account);
}
