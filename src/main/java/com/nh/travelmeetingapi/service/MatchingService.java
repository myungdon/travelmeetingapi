package com.nh.travelmeetingapi.service;

import com.nh.travelmeetingapi.entity.matching.Matching;
import com.nh.travelmeetingapi.model.matching.MatchingCreateRequest;
import com.nh.travelmeetingapi.model.matching.MatchingItem;
import com.nh.travelmeetingapi.model.matching.MatchingResponse;
import com.nh.travelmeetingapi.model.matching.MatchingTypeChangeRequest;
import com.nh.travelmeetingapi.repository.MatchingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.substring;

@Service
@RequiredArgsConstructor
public class MatchingService {
    private final MatchingRepository matchingRepository;

    public void setMatching(MatchingCreateRequest request){
        Matching addData = new Matching();
        addData.setMatchingCode(request.getMatchingCode());
        addData.setMatchingPrice(request.getMatchingPrice());
        addData.setMatchingCategory(substring(request.getMatchingCode(), 0, 1));
        addData.setDateStart(LocalDate.parse(substring(request.getMatchingCode(), 1, 11)));
        addData.setDateEnd(request.getDateEnd());
        addData.setAddress(substring(request.getMatchingCode(), 11, 13));
        addData.setPension(substring(request.getMatchingCode(), 13));

        matchingRepository.save(addData);
    }

    public List<MatchingItem> getMatchings(){
        List<Matching> originList = matchingRepository.findAll();

        List<MatchingItem> result = new LinkedList<>();

        for (Matching matching : originList){
            MatchingItem addItem = new MatchingItem();
            addItem.setId(matching.getId());
            addItem.setMatchingCode(matching.getMatchingCode());
            addItem.setMatchingPrice(matching.getMatchingPrice());
            addItem.setMatchingCategory(matching.getMatchingCategory());
            addItem.setDateStart(matching.getDateStart());
            addItem.setDateEnd(matching.getDateEnd());
            addItem.setAddress(matching.getAddress());
            addItem.setPension(matching.getPension());

            result.add(addItem);
        }

        return result;
    }

    public MatchingResponse getMatching(long matchingId){
        Matching originData = matchingRepository.findById(matchingId).orElseThrow();

        MatchingResponse response = new MatchingResponse();
        response.setId(originData.getId());
        response.setMatchingCode(originData.getMatchingCode());
        response.setMatchingPrice(originData.getMatchingPrice());
        response.setMatchingCategory(originData.getMatchingCategory());
        response.setDateStart(originData.getDateStart());
        response.setDateEnd(originData.getDateEnd());
        response.setAddress(originData.getAddress());
        response.setPension(originData.getPension());

        return response;
    }

    public void putMatchingType(long matchingId, MatchingTypeChangeRequest request){
        Matching originData = matchingRepository.findById(matchingId).orElseThrow();
        originData.setMatchingCode(request.getMatchingCode());
        originData.setMatchingPrice(request.getMatchingPrice());
        originData.setDateEnd(request.getDateEnd());

        matchingRepository.save(originData);
    }

    public void delMatching(long matchingId){

        matchingRepository.deleteById(matchingId);
    }
}
